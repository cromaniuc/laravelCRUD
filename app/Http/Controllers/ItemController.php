<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::all();
        return view('items', ['items' => $items]);
    }

    public function create ()
    {
      return view('formitem');
    }
     
    public function store (Request $request)
    {
      $data = $request->validate ([
        'name' => 'required|min:5',
        'description' => 'required|min:8',
        'price' => 'required|numeric|min:0'
      ]);
     
      Item::create($data);
      return redirect('/index');
    }

    public function updateForm ($id)
    {
        $item = Item::find($id);   
        return view('formitem', ['item' => $item]);
    }     
    
    public function update (Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:5',
            'description' => 'required|min:8',
            'price' => 'required|numeric|min:0'
        ]);
        
        $id = $request->input('id'); 
        
        $item = Item::find($id);
        $item->name = $request->input('name'); 
        $item->description = $request->input('description'); 
        $item->price = $request->input('price');
        $item->save();
        
        return redirect('/index');
    }
}
