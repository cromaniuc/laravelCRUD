<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    // proteccion de injectciones, solo permite modificar los campos especificados
    protected $fillable = ['name', 'description', 'price'];
    public $timestamps = false;
}