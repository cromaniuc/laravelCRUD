@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ $item->price }}€</td>
                        <td><a href="item/update/{{$item->id}}" class="btn btn-info">Update</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            <a href="item/create" class="btn btn-primary">New Item</a>
        </div>
    </div>
</div>
@endsection
